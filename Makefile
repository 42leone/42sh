LIBDIR = slib

COREDIR = core

BINDIR = bin/

BINSRC = core/42sh

all:
	make -C $(LIBDIR)
	make -C $(COREDIR)
	mkdir -p $(BINDIR)
	cp $(BINSRC) $(BINDIR)

clean:
	make clean -C $(LIBDIR)
	make clean -C $(COREDIR)

fclean:
	make fclean -C $(LIBDIR)
	make fclean -C $(COREDIR)
	rm -rf $(BINDIR)

re: fclean all

.phony: clean fclean re $(NAME)
