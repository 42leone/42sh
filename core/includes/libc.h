#ifndef LIBC_H
# define LIBC_H

# include <unistd.h>
# include <stdlib.h>

char	*ft_strtrim(const char *str);
size_t	ft_strlen(const char *str);
char	*ft_strdup(char *src);
char	*ft_strcat(char *s1, char *s2);
char	ft_strncmp(char *s1, char *s2, size_t len);
char	**ft_strsplit(const char *s, char c);
void	ft_error(char *msg);
void	ft_putstr(char *msg);
void	ft_bzero(void *buff, size_t len);
char	*ft_strcpy(char *dest, char *s1);
char	*ft_strnew(size_t size);
void	*ft_memalloc(size_t size);
void	*ft_memcpy(void *dest, const void *src, size_t size);
int		ft_isalnum(int c);
int		ft_isdigit(int c);
int		ft_isalpha(int c);
void	ft_putchar(const char c);
void	ft_putstr_fd(char const *s, int fd);
char	samestr(const char *s1, const char *s2);
void	*ft_memchr(const void *str, int c, size_t len);

#endif
