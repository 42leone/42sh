#ifndef RC_H
# define RC_H

typedef struct	s_split	t_split;
typedef struct	s_rc	t_rc;
typedef struct	s_op	t_op;
typedef t_rc	*(*ptrc)(char *str, t_rc *rc);
typedef	int	(*color)(char *str);

struct		s_op
{
	int	ac;
	int	color;
};

struct		s_rc
{
	int	uac;
	int	lac;
	int	pac;
	int	color;
};

struct	s_split
{
	char	**tmp;
	t_split	*next;
};

#endif
