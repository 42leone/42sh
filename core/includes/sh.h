#ifndef SH_H
# define SH_H

# define __DEBUG_LEXER__	0
# define __DEBUG_PARSING__	0
# define __DEBUG_EXEC__		0
# define __DEBUG_MAPEXE__	0

# include "slib.h"
# include <sh_parser.h>
# include <sh_lexer.h>
# include <curses.h>
# include <sys/wait.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <term.h>
# include <dirent.h>
# include <rc.h>
//# include <termios.h>
# include <stdio.h> // PRINTF !!

# define un __attribute__ ((unused))
# define BUFF_SIZE 5

# define KILL_SEGV "Segmentation fault"
# define KILL_FPE "Floating exception"
# define KILL_INT "Interrupted"
# define KILL_BUS "Bus error"
# define KILL_QUIT "Quit"
# define KILL_SIGNAL "Process kill by signal"
# define FILECONF ".42shrc"
# define FILEHIST ".42history"

# define STDIN			0
# define STDOUT			1
# define ERR_MALLOC		-1
# define ERR_FORK		-5
# define ERR_SETPGID	-6
# define ERR_TERM		-7
# define ERR_LEXING		-8
# define ERR_PARSING	-9

# define KEYLEFT	4479771
# define KEYRIGHT	4414235
# define KEYUP		4283163
# define KEYDOWN	4348699
# define ENTER		13
# define TAB		9

typedef struct	s_env		t_env;
typedef struct	s_exe		t_exe;
typedef struct	s_read		t_read;
typedef struct	s_user		t_user;
typedef struct	s_sh		t_sh;
typedef struct	s_exe_fd	t_exe_fd;
typedef struct	s_proc		t_proc;
typedef struct	s_job		t_job;

typedef struct	s_char		t_char;

typedef char	(*build)(t_sh *sh, char **arg);

/*
 * List of all alive shell processes, minus the first one.
 */

struct		s_job
{
	t_proc	*s_proc;
	t_job	*next;
	t_job	*prev;
};

struct		s_proc
{
	int		pid;
	t_proc	*next;
};

struct		s_env
{
	char		*line;
	t_env	*next;
};

struct			s_exe
{
	char		*pexe;
	int			ac;
	t_exe		*next[256];
};

struct		s_char
{
	char	c;
	t_char	*next;
	t_char	*prev;
};

struct		s_read
{
	char	buff[BUFF_SIZE];
	size_t	len;
	t_read	*next;
	t_read	*prev;
};

struct		s_user
{
	t_rc	rc;
	char	ac;
	char	*user;
	char	*path;
	char	*pwd;
	char	*home;
	char	*term;
	char	*oldpwd;
	char	*phist;
	char	*pshrc;
};

struct		s_sh
{
	t_env	*env;
	t_proc	*proc;
	t_exe	*mapexe;
	t_user	user;
	t_job	*jobs;
	int		pgid;
	int		exit;
	struct termios	*term_save;
	char	ae;
};

/*
 * I/O control (fd_in / fd_out -> current fd in use
 * in_bk / out_bk -> backup fd);
 */
struct		s_exe_fd
{
	int		fd_in;
	int		fd_out;
	int		in_bk;
	int		out_bk;
	t_job	*job;
};

/*
 * READ FROM COMMAND LINE
 */
char	*ft_readcmd(void);
void	ft_freeread(t_read *s);

/*
 * CONFIG FILE
 */
t_sh	*pathuser(t_sh *sh);
char	ft_shrc(t_sh *sh);

int		aff_prompt(t_sh *sh);

/*
 * ENV
 */
t_env	*r_env_ptr(t_env *env, char *toseek);
t_env	*ft_cpyenv(char **env);
char	*ft_getenv(t_env *env, char *toseek);
//char	loadrc(t_sh *sh);

/*
 * MAP EXE
 */
t_exe	*addexe(t_exe *mapexe, unsigned char *exe, int ac);
t_sh	*addbuiltin(t_sh *sh);
t_exe	*init_exe(t_exe *in);
char	ft_mapexe(t_sh *sh);
int		get_pexe(t_sh *sh, t_cmd *cmd);

/*
 * BUILTINS
 */
build	*init_buil_ptr(void);
char	s_cd(t_sh *sh, char **arg);
char	s_env(t_sh *sh, char **arg);
char	s_exit(t_sh *sh, char **arg);
/*
 ** EXEC FUNTIONS
 */

int		sh_exec_bkg(t_cmd *s_cmd, t_exe_fd *s_exe, t_sh *s_sh, int bkg);

int		sh_exec_cmd(t_cmd *s_cmd, t_sh *sh, t_exe_fd *s_exe);
int		sh_exec_redi(t_cmd *s_cmd, t_exe_fd *s_exe);
int		sh_exec_redo(t_cmd *s_cmd, t_exe_fd *s_exe);
int		sh_exec_pipe(t_cmd *s_cmd, t_exe_fd *s_exe, t_sh *sh, int bkg);

t_job	*new_job(t_sh *s_sh);
char	push_exe(unsigned char *exe, t_exe *s_exe, char *src, int flag);

#endif
