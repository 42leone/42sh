/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_lexer.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/18 16:38:44 by ccano             #+#    #+#             */
/*   Updated: 2014/02/18 16:39:48 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SH_LEXER_H
# define SH_LEXER_H

# include <fsm.h>

# define S_INIT		0
# define S_ALPHA	1
# define S_REDI		2
# define S_REDO		3
# define S_PIPE		4
# define S_SPACE	5
# define S_SEP		6
# define S_BKG		7
# define S_END		8
# define S_ELSE		9

int				sh_lexer(char *line, t_lexem **s_blexem);

#endif

