#include "sh.h"

char	*create_src(char *dir, char *name)
{
	size_t	cn[2] = {-1, -1};
	char	*new;

	if (name == NULL)
		return (0);
	if (dir == NULL)
		return (0);
	if (!(new = malloc(sizeof(*new) * (ft_strlen(dir) + ft_strlen(name) + 2))))
		return (0);
	while (dir[++cn[0]])
		new[cn[0]] = dir[cn[0]];
	new[cn[0]] = '/';
	while (name[++cn[1]])
		new[++cn[0]] = name[cn[1]];
	new[++cn[0]] = '\0';
	return (new);
}

t_env		*cpy_env(char **env)
{
	size_t	count;
	t_env	*ret;
	t_env	*tmp;

	if (!*env)
		return (0);
	if (!(tmp = malloc(sizeof(*tmp))))
		return (0);
	count = -1;
	ret = tmp;
	while (env[++count])
	{
		if (!(tmp->line = ft_strdup(env[count])))
			return (0);
		if (env[count + 1])
		{
			if (!(tmp->next = malloc(sizeof(*tmp))))
				return (0);
			tmp = tmp->next;
		}
	}
	tmp->next = NULL;
	return (ret);
}

char		*ft_getenv(t_env *env, char *toseek)
{
	t_env	*tmp;
	size_t	len;

	len = ft_strlen(toseek);
	tmp = env;
	while (tmp)
	{
		if ((ft_strncmp(tmp->line, toseek, len)))
			return (&tmp->line[len]);
		tmp = tmp->next;
	}
	return (0);
}

char		load_user(t_user *s_user, t_env *env)
{
	if (!(s_user->user = ft_getenv(env, "USER=")))
		return (0);
	if (!(s_user->pwd = ft_getenv(env, "PWD")))
		return (0);
	if (!(s_user->path = ft_getenv(env, "PATH=")))
		return (0);
	if (!(s_user->term = ft_getenv(env, "TERM=")))
		return (0);
	if (!(s_user->oldpwd = ft_getenv(env, "OLDPWD=")))
		return (0);
	if (!(s_user->home = ft_getenv(env, "HOME=")))
		return (0);
	return (1);
}

char	push_exe(unsigned char *exe, t_exe *s_exe, char *src, int flag)
{
	if (!*exe)
	{
		if (!flag)
		{
			s_exe->pexe = src;
			s_exe->ac = 0;
		}
		else
		{
			s_exe->pexe = NULL;
			s_exe->ac = flag;
		}
		return (1);
	}
	if (!(s_exe->next[*exe]))
	{
		if (!(s_exe->next[*exe] = malloc(sizeof(*s_exe))))
			return (0);
		ft_bzero(s_exe->next[*exe], sizeof(**s_exe->next));
		return (push_exe(exe + 1, s_exe->next[*exe], src, flag));
	}
	return (push_exe(exe + 1, s_exe->next[*exe], src, flag));
}

char	is_hidden(const char const *exe)
{
	size_t	count;

	count = -1;
	while (exe[++count])
	{
		if ((exe[count] == '.' && exe[count + 1] == 0)
			|| (exe[count] == '.' && exe[count + 1] == '.'
				&& exe[count + 2] == 0))
			return (1);
	}
	return (0);
}

char	maping_exe(t_sh *sh, char **path)
{
	DIR				*dir;
	struct dirent	*name;
	size_t			cn;
	char			*tmp;

	cn = -1;
	while (path[++cn])
	{
		if (!(dir = opendir(path[cn])))
			continue;
		while ((name = readdir(dir)))
		{
			if ((is_hidden(name->d_name)))
				continue;
			if (!(tmp = create_src(path[cn], name->d_name)))
				return (0);
			if (!(push_exe((unsigned char *)name->d_name, sh->mapexe, tmp, 0)))
				return (0);
		}
		free(path[cn]);
		closedir(dir);
	}
	free(path);
	return (1);
}

char		add_builtins(t_sh *s_sh)
{
	if (!(push_exe((unsigned char *)"cd", s_sh->mapexe, NULL, 2)))
		return (0);
	if (!(push_exe((unsigned char *)"env", s_sh->mapexe, NULL, 3)))
		return (0);
	if (!(push_exe((unsigned char *)"exit", s_sh->mapexe, NULL, 4)))
		return (0);
	return (1);
}

char		init_mapexe(t_sh *s_sh)
{
	if (!(s_sh->mapexe = malloc(sizeof(*s_sh->mapexe))))
		return (0);
	ft_bzero(s_sh->mapexe, sizeof(*s_sh->mapexe));
	return (1);
}

char		src_conf(t_user *s_user)
{
	char				*tmp;
	size_t				count;
	size_t				counter;
	const char const	*conf = FILECONF;

	if (!(tmp = malloc(sizeof(*tmp) * (ft_strlen(s_user->home) + ft_strlen(conf) + 2))))
		return (0);
	count = -1;
	counter = -1;
	while (s_user->home[++count])
		tmp[++counter] = s_user->home[count];
	tmp[++counter] = '/';
	count = -1;
	while (conf[++count])
		tmp[++counter] = conf[count];
	tmp[++counter] = 0;
	s_user->pshrc = tmp;
	return (1);
}

char		load_everything(t_sh *s_sh, char **env)
{
	char	**path;


	if (!(s_sh->env = cpy_env(env)))
		return (print(STDERR, "ENV NULL\n", 0));
	if (!(load_user(&s_sh->user, s_sh->env)))
		return (0);
	if (!(init_mapexe(s_sh)))
		return (0);
	if (!(path = ft_strsplit(s_sh->user.path, ':')))
		return (0);
	if (!(maping_exe(s_sh, path)))
		return (0);
	if (!(add_builtins(s_sh)))
		return (0);
	if (!(src_conf(&s_sh->user)))
		return (0);
	return (1);
}

int			main(int ac, char **av, char **env)
{
	t_sh	s_sh;

	(void)ac;
	(void)av;
	if (!(load_everything(&s_sh, env)))
		return (0);
	print(STDOUT, "EVERYTHING LOADED !\n", 1);
	return (0);
}
