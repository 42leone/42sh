#include "slib.h"

void	ft_bzero(void *buff, size_t len)
{
	char	*tmp;

	tmp = (char *)buff;
	while (len--)
		tmp[len] = '\0';
}
