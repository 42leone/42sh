#include <slib.h>

char	*ft_strcat(char *s1, char *s2)
{
	size_t	count[2] = {-1, -1};

	while (s1[++count[0]])
		;
	while (s2[++count[1]])
		s1[count[0]++] = s2[count[1]];
	s1[count[0]] = '\0';
	return (s1);
}
