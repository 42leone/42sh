#include "slib.h"

char	*ft_strcpy(char *dest, char *s1)
{
	size_t	cn;

	cn = -1;
	while (s1[++cn])
		dest[cn] = s1[cn];
	dest[cn] = '\0';
	return (dest);
}
