/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/24 02:09:48 by ccano             #+#    #+#             */
/*   Updated: 2013/12/01 23:00:09 by klo              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "slib.h"

char	*ft_strdup(char *str)
{
	unsigned int	size;
	char		*str_cpy;

	str_cpy = NULL;
	if (str)
	{
		size = ft_strlen(str) + 1;
		if ((str_cpy = malloc(sizeof(*str) * size)) == NULL)
			return (NULL);
		while (size >= 1)
		{
			str_cpy[size - 1] = str[size - 1];
			--size;
		}
	}
	return (str_cpy);
}
