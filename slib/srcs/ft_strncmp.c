#include "slib.h"

char	ft_strncmp(char *s1, char *s2, size_t len)
{
	size_t	cn;

	cn = -1;
	while (++cn < len && s2 && s1 && s1[cn] == s2[cn])
		;
	if (cn == len)
		return (1);
	return (0);
}
