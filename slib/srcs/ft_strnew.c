/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/24 04:35:52 by ccano             #+#    #+#             */
/*   Updated: 2013/12/02 04:47:17 by klo              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "slib.h"

char		*ft_strnew(size_t size)
{
	char	*buf;

	if (size < 1)
		return (NULL);
	else if ((buf = ft_memalloc(size + 1)) == NULL)
		return (NULL);
	//buf = ft_memset(buf, '\0', size + 1);
	return (buf);
}

